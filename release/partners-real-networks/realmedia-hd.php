<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RealMedia HD - Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-rmhd.php'; ?>
        <section class="page-intro rmhd-intro">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <p>RealMedia™ HD, the successor to RealNetworks RMVB video codec, delivers stunning image quality for today’s and tomorrow’s media experiences. 
                        RealMedia HD is engineered to provide optimal results for ultra-high-definition content, including 1080p, 4K and even 8K.</p>
                </div>
            </div>
        </section>
        
        <section class="rmhd-design-content grey">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Designed for the future</h1>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>Huge bandwidth savings.</h2>
                        <p>Stream 1080p and 4K content at half the bitrate while  drastically reducing encoding and storage costs.</p>
                    </div>
                    <div class="column-5 offset-2">
                        <h2>Quality delivery on current networks.</h2>
                        <p>Ensure faithful reproduction of 1080p and 4K content when streamed over current-generation networks.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>Efficient hardware utilization.</h2>
                        <p>RealMedia HD’s low-complexity code minimizes CPU load, heat and battery drain.</p>
                    </div>
                    <div class="column-5 offset-2">
                        <h2>Seamless device integration</h2>
                        <p>RealMedia HD SDK supports Windows, iOS and Android devices.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>High quality with few compromises.</h2>
                        <p>RealMedia HD provides visibly higher quality at any given bitrate vs. H.264, H.265 and VP9.</p>
                    </div>
                    <div class="column-5 offset-2">
                        <h2>Support for next-generation media.</h2>
                        <p>Prepare for the future with a codec designed for 1080p, 4K, 8K, 360°, VR and more. </p>
                    </div>
                </div>
            </div>
        </section>
<!--        <section class="quote-cta rmhd">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <div class="quote"></div>
                    <div class="source"> </br></div>
                </div>
            </div>
        </section>-->
        <section class="rmhd-audience-content">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <img src="assets/images/rmhd-icon7.png" data-rjs="2" alt="Gears Icon"/>
                    <h2>Content creators</h2>
                    <p>Ensure reproduction of 1080p and 4K content with visibly higher quality at any bitrate streamed over current-generation networks.</p>
                </div>
                <div class="column-10 offset-1">
                    <img src="assets/images/rmhd-icon8.png" data-rjs="2" alt="Gears Icon"/>
                    <h2>Content distributors</h2>
                    <p>Deliver premium-quality content experiences with fewer TLS streams and significant savings on distribution, encoding and storage costs.</p>
                </div>
                <div class="column-10 offset-1">
                    <img src="assets/images/rmhd-icon9.png" data-rjs="2" alt="Gears Icon"/>
                    <h2>OEMs</h2>
                    <p>Get crystal-clear 1080p and 4K decoding, even on modest hardware.</p>
                </div>
            </div>
        </section>
        <section class="questions-cta real">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Have Questions?</h1>
                    <div class="copy">Learn more about RealMedia HD. Email <a href="mailto:RMHD_Partners@realnetworks.com">RMHD_Partners@realnetworks.com</a>. </div>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>