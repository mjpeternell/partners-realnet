<section class="rmhd-deliver-content">
    <div class="inner-row">
        <div class="column-10 offset-1">
            <h1>Deliver faster, lighter videos</h1>
        </div>
        <div class="column-10 offset-1">
            <div class="column-4">
                <img src="assets/images/rmhd-icon1.png" data-rjs="2" alt="Gears Icon"/>
                <p>Greater compression efficiency—up to 50% lower bitrate than H.264</p>
            </div>
            <div class="column-4">
                <img src="assets/images/rmhd-icon2.png" data-rjs="2" alt="Bolt Icon"/>
                <p>Smaller file size and faster downloads</p>
            </div>
            <div class="column-4">
                <img src="assets/images/rmhd-icon3.png" data-rjs="2" alt="Monitor Icon"/>
                <p>Support for next-generation media such as 4K Ultra HD, 8K and more </p>
            </div>
        </div>
        <div class="column-10 offset-1">

            <div class="column-4">
                <img src="assets/images/rmhd-icon4.png" data-rjs="2" alt="HD Icon"/>
                <p>Reduced storage needs<p>
            </div>
            <div class="column-4">
                <img src="assets/images/rmhd-icon5.png" data-rjs="2" alt="Battery Icon"/>
                <p>Lower CPU usage and less battery consumption </p>
            </div>
            <div class="column-4">
                <img src="assets/images/rmhd-icon6.png" data-rjs="2" alt="Tablet Icon"/>
                <p>Crisp and clear playback on any device</p>
            </div>
        </div>
    </div>
</section>