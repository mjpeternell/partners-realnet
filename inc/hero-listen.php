<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="homeHero" class="hero alt listen-now full-100">
    <img src="assets/images/listen-header.png" data-rjs="2" class="img-responsive"/>
    <div class="caption">
        <h1><img src="assets/images/listen-logo-white.png" data-rjs="2" alt="Listen"/></h1>
        <h4>Maximize ARPU with the industry’s best ringback services</h4>
        <h5>Modern ringback messaging</h5>
    </div>
</section>