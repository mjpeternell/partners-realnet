<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="homeHero" class="hero alt rt-gradient">
    <img src="assets/images/real-times-header.png" data-rjs="2" class="img-responsive"/>
    <div class="caption">
        <h1><img src="assets/images/realtimes-logo-white.png" data-rjs="2" alt="RealTimes"/></h1>
        <h4>Increase subscriber engagement and retention</h4>
        <h5>Intelligent photo and video services</h5>
    </div>
</section>