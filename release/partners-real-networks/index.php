
<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-home.php'; ?>
        <section class="home-cta real-times">
            <div class="inner-row">
                <div class="column-7 offset-1">
                    <h1><img src="assets/images/realtimes-logo.png" data-rjs="2" alt="RealTimes"/></h1>
                    <h2>Increase subscriber engagement and retention</h2>
                    <h3>Intelligent photo and video services</h3>
                    <p>RealTimes, the popular photo and video technology lets you deliver more of what customers want while adding value to data plans and increasing revenue. Consumers can’t get enough of RealTimes – personalized video stories and collages, easy social sharing and plenty of storage for life’s most important memories.</p>
                    <a href="/realtimes" class="blue-btn" title="Learn More">Learn More</a>
                </div>
            </div>
        </section>
        <section class="home-cta rmhd">
            <div class="inner-row">
                <div class="column-7 offset-1">
                    <h1><img src="assets/images/rmhd-logo.png" data-rjs="2" alt="RealMedia HD"/></h1>
                    <h2>Play tomorrow’s content on today’s networks and devices</h2>
                    <h3>High-efficiency mobile video compression</h3>
                    <p>RealMedia™ HD, the successor to RealNetworks RMVB video codec, delivers stunning image quality for today’s and tomorrow’s media experiences. RealMedia HD is engineered to provide optimal results for ultra-high-definition content, including 1080p, 4K and even 8K.</p>
                    <a href="/realmedia-hd" class="blue-btn" title="Learn More">Learn More</a>
                </div>
            </div>
        </section>
        <section class="home-cta listen">
            <div class="inner-row">
                <div class="column-7 offset-1">
                    <h1><img src="assets/images/listen-logo.png" data-rjs="2" alt="Listen"/></h1>
                    <h2>Create a carrier-branded sales and marketing channel<br>to generate sales</h2>
                    <h3>Profitable ringback messaging</h3>
                    <p>The LISTEN platform moves beyond simple ringback tones and allows carriers to engage their subscribers using targeted ringback tones and customized messages informing their customers about new services, special promotions and offers. Because carriers have detailed account information at their fingertips, they’re able to deliver custom offers at exactly the right time. </p>
                    <a href="/listen" class="blue-btn" title="Learn More">Learn More</a>
                </div>
            </div>
        </section>
        <section class="home-cta napster">
            <div class="inner-row">
                <div class="column-7 offset-1">
                    <h1><img src="assets/images/napster-home-logo.png" data-rjs="2" alt="Napster"/></h1>
                    <h2>Integrate rich music experiences to drive new revenue streams</h2>
                    <h3>Premium music streaming service</h3>
                    <p>Create new revenue streams by offering your customers rich and engaging music services that connect emotionally with users. Napster’s music service is easily integrated and provides over 40 million songs and exclusive content your customers can access.</p>
                    <a href="/napster" class="blue-btn" title="Learn More">Learn More</a>
                </div>
            </div>
        </section>
        <section class="partners">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Our Partners</h1>
                    <ul class="partners-list">
                        <li class="partner"><img src="assets/images/verizon-logo.png" alt="Verizion" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/idea-logo.png" alt="Idea" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/funambol-logo.png" alt="Funambol" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/tmobile-logo.png" alt="T Mobile" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/huawei-logo.png" alt="Huawei" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/kddi-logo.png" alt="KDDI" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/vodafone-logo.png" alt="Vodafone" data-rjs="2"/></li>
                        <li class="partner"><img src="assets/images/vivo-logo.png" alt="Vivio" data-rjs="2"/></li>                        
                    </ul>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>