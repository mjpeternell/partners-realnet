
<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RealTimes - Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-real-times.php'; ?>
        <section class="page-intro">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <p>RealTimes, the popular photo and video technology lets you deliver more of what customers want while adding value to data plans and increasing revenue. Consumers can’t get enough of RealTimes – personalized video stories and collages, easy social sharing, and plenty of storage for life’s most important memories.</p>
                </div>
            </div>
        </section>
        <section class="rt-give-content">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>GIVE YOUR CUSTOMERS WHAT THEY’RE LOOKING FOR</h1>
                </div>
                <div class="column-5 offset-1">
                    <img src="assets/images/realtimes1.png" data-rjs="2" alt="Mobile Phone"/>
                </div>
                <div class="column-5">
                    <h2>Automatic video stories and photo collages</h2>
                </div>
            </div>
        </section>
        <section class="rt-give-content">
            <div class="inner-row">
                <div class="column-5 offset-1">
                    <img src="assets/images/realtimes2.png" data-rjs="2" alt="Mobile Phone"/>
                </div>
                <div class="column-5">
                    <h2>Customization with music, narration and design </h2>
                </div>
            </div>
        </section>
        <section class="rt-give-content">
            <div class="inner-row">
                <div class="column-5 offset-1">
                    <img src="assets/images/realtimes3.png" data-rjs="2" alt="Mobile Phone"/>
                </div>
                <div class="column-5">
                    <h2>Instant sharing by email, text or social media</h2>
                </div>
            </div>
        </section>
        <section class="rt-give-content">
            <div class="inner-row">
                <div class="column-5 offset-1">
                    <img src="assets/images/realtimes4.png" data-rjs="2" alt="Mobile Phone"/>
                </div>
                <div class="column-5">
                    <h2>Photo prints, books, phone cases and more, all ordered right from the app</h2>
                </div>
            </div>
        </section>
        <section class="rt-give-content">
            <div class="inner-row">
                <div class="column-5 offset-1">
                    <img src="assets/images/realtimes5.png" data-rjs="2" alt="Mobile Phone"/>
                </div>
                <div class="column-5">
                    <h2>Playback optimized for device and bandwidth</h2>
                </div>
            </div>
        </section>
        <section class="quote-cta realtime">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <div class="quote">"Vodafone Stories made by RealTimes, is a great addition to our service, encouraging our customers to turn their memories 
                        into engaging stories and then share them using Vodafone’s fast and reliable mobile networks."</div>
                    <div class="source">Stefano Parisse, </br>Consumer Services Director, Vodafone Group</div>
                </div>
            </div>
        </section>
        <section class="rt-grow-content first">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>GROW YOUR BUSINESS WITH REALTIMES</h1>
                </div>
                <div class="column-6 offset-3">
                    <img src="assets/images/icon1.png" data-rjs="2" alt="Icon 1"/>
                    <div class="grow-title">Increase customer engagement</div>
                    <div class="grow-copy">With services they can’t live without, you’ll 
                        keep customers engaged and counting on you for their precious stories.
                    </div>
                </div>
            </div>
        </section>
        <section class="rt-grow-content">
            <div class="inner-row">
                <div class="column-6 offset-3">
                    <img src="assets/images/icon2.png" data-rjs="2" alt="icon 2"/>
                    <div class="grow-title">Drive revenue growth</div>
                    <div class="grow-copy">Fun, interactive photo and video features let you 
                        create new revenue streams and drive data usage to raise the lifetime value of 
                        your customers.</div>
                </div>
            </div>
        </section>
        <section class="rt-grow-content">
            <div class="inner-row">
                <div class="column-6 offset-3">
                    <img src="assets/images/icon3.png" data-rjs="2" alt="icon 3"/>
                    <div class="grow-title">Make personal clouds more valuable</div>
                    <div class="grow-copy">Leverage the interactive features in 
                        RealTimes to augment your personal cloud offerings.
                    </div>
                </div>
            </div>
        </section>
        <section class="rt-grow-content">
            <div class="inner-row">
                <div class="column-6 offset-3">
                    <img src="assets/images/icon4.png" data-rjs="2" alt="icon 3"/>
                    <div class="grow-title">Find the perfect fit</div>
                    <div class="grow-copy">Seamlessly integrate our technology into your current backup service 
                        using the RealTimes SDK.
                    </div>
                </div>
            </div>
        </section>
        <section class="rt-grow-content last">
            <div class="inner-row">
                <div class="column-6 offset-3">
                    <img src="assets/images/icon5.png" data-rjs="2" alt="icon 3"/>
                    <div class="grow-title">Count on our expertise</div>
                    <div class="grow-copy">With millions of RealTimes customers and partnerships 
                        with leading mobile carriers, RealNetworks is the reliable, proven source for 
                        photo and video technology.
                    </div>
                </div>
            </div>
        </section>
        <section class="questions-cta quest">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Have Questions?</h1>
                    <div class="copy">Learn more about intelligent photo and video services from RealTimes. Email  <a href="mailto:RealTimes_Partners@realnetworks.com">RealTimes_Partners@realnetworks.com</a>. </div>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>