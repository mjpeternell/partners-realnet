<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="homeHero" class="hero alt napster">
    <img src="assets/images/napster-header.png" data-rjs="2" class="img-responsive"/>
    <div class="caption">
        <h1><img src="assets/images/napster-logo-white.png" data-rjs="2" alt="Napster"/></h1>
        <h4>Partner with Napster</h4>
        <h5>Give your customers the best music experience</h5>
    </div>
</section>