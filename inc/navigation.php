<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="navigation" role="banner">
    <div class="navigation-wrapper">
        <a href="/" class="logo">
            <img src="/assets/images/realnetworks-logo.png" data-rjs="2" alt="RealNetworks">
        </a>
        <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
            <span class="hamburger-bar"></span>
            <span class="hamburger-bar"></span>
        </a>
        <nav role="navigation">
            <ul id="js-navigation-menu" class="navigation-menu show">
                <li class="nav-link"><a href="/">Partners Home</a></li>
                <li class="nav-link"><a href="/realtimes">RealTimes</a></li>
                <li class="nav-link"><a href="/realmedia-hd">RealMedia HD</a></li>
                <li class="nav-link"><a href="/listen">LISTEN</a></li>
                <li class="nav-link"><a href="/napster">Napster</a></li>
                <li class="nav-link"><a href="/contact-us">Contact Us</a></li>
            </ul>
        </nav>
    </div>
</header>
