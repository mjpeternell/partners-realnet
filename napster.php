
<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-napster.php'; ?>
        <section class="rmhd-design-content grey bundles">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1><strong>Bundles</strong> | Add a music bundle to your product.  </h1>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-1">
                        <img src="assets/images/napster-icon1.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Earn money by giving your customers 2 months of free music with the purchase of your product and get paid for every paying subscriber.</p>
                    </div>
                    <div class="column-1">
                        <img src="assets/images/napster-icon2.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Add value bundling 2 months of Napster Premier free with your product at retail. We provide you with a unique URL for your customers to sign up with.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-1">
                        <img src="assets/images/napster-icon3.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Amp up your marketing with creative assets, such as stickers or inserts that you can customize. Simply print and distribute through your marketing channels to promote the offer.</p>
                    </div>
                    <div class="column-1">
                        <img src="assets/images/napster-icon4.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Receive dedicated reports and payment information sent to you monthly.</p>
                    </div>
                </div>
                <div class="column-10 offset-1 top-margin-100">
                    <h1><strong>Installers</strong> | Bring millions of songs to every home.  </h1>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-1">
                        <img src="assets/images/napster-icon5.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Add value to your services by offering your customers millions of songs in high-quality audio (320kbps) to complete their home audio system installation.</p>
                    </div>
                    <div class="column-1">
                        <img src="assets/images/napster-icon6.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Earn a commission on all qualifying trials and subscriptions that you refer.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-1">
                        <img src="assets/images/napster-icon7.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Amp up your marketing with ready to print collateral and creative assets.</p>
                    </div>
                    <div class="column-1">
                        <img src="assets/images/napster-icon8.png" data-rjs="2" alt="Quote"/>
                    </div>
                    <div class="column-5">
                        <p>Receive dedicated reports and payment information sent to you monthly.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="questions-cta nap">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Have Questions?</h1>
                    <div class="copy">We’d love to hear from you. Get in touch with our experts at <a href="mailto:api_inquiries@napster.com">api_inquiries@napster.com</a>. </div>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>