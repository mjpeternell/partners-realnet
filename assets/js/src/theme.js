/*! Panorama - panorama.js - v0.1.0 - 2016-04-01
 * http://panoramaDotcom
 * Copyrigh0;
 jQuery(document).ready(function () {t (c) 2016; * Licensed GPLv2+ */

$(window).resize(function () {
    var more = document.getElementById("js-navigation-more");
    if ($(more).length > 0) {
        var windowWidth = $(window).width();
        var moreLeftSideToPageLeftSide = $(more).offset().left;
        var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;

        if (moreLeftSideToPageRightSide < 330) {
            $("#js-navigation-more .submenu .submenu").removeClass("fly-out-right");
            $("#js-navigation-more .submenu .submenu").addClass("fly-out-left");
        }

        if (moreLeftSideToPageRightSide > 330) {
            $("#js-navigation-more .submenu .submenu").removeClass("fly-out-left");
            $("#js-navigation-more .submenu .submenu").addClass("fly-out-right");
        }
    }
});

$(document).ready(function () {
    
    // Add current page css styling to main nav.
    $(function () {
        var loc = window.location.pathname;

        if (loc === '/') {
            // Index (home) page
            $('nav li:first-child a').addClass('current');
        } else {
            // Other page
            $('nav a[href^="/' + loc.split("/")[1] + '"]').addClass('current');
        }
    });

     /*
     * Retinajs image
     */
    retinajs($('img'));
    //retinajs(document.querySelectorAll('img'));
    // Attempts to process only the elements in the collection.
    // Each one still needs to be marked with `data-rjs` and
    // will still be ignored if it has already been processed.
    
    /*
     * Nav Menu functionality
     */
    
    var menuToggle = $("#js-mobile-menu").unbind();
    $("#js-navigation-menu").removeClass("show");

    menuToggle.on("click", function (e) {
        e.preventDefault();
        $("#js-navigation-menu").slideToggle(function () {
            if ($("#js-navigation-menu").is(":hidden")) {
                $("#js-navigation-menu").removeAttr("style");
            }
        });
    });

    /*
     * Scroll-to-top Functionality
     */

    //Scroll to top of page
//    $(window).scroll(function () {
//        var my_docHeight = $(document).height();
//        var my_winHeight = $(window).height();
//        if ($(this).scrollTop() > 100) {
//            $('.scroll-to-top').fadeIn();
//        } else {
//            $('.scroll-to-top').fadeOut();
//        }
//        if ($(this).scrollTop() + my_winHeight === my_docHeight) {
//            $('.scroll-to-top').fadeOut();
//        }
//    });

//Click event to scroll to top
    $('.scroll-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });
});

