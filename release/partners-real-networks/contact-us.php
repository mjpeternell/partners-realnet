
<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Contact Us - Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-contact.php'; ?>
        <section class="contact-cta default real-times first">
            <div class="inner-row">
                <div class="column-3 offset-1">
                    <img src="assets/images/realtimes-logo.png" data-rjs="2" alt="RealTimes"/>
                </div>
                <div class="column-7">
                    <a href="mailto:RealTimes_Partners@realnetworks.com" class="link-btn" title="Contact">RealTimes_Partners@realnetworks.com</a>
                </div>
            </div>
        </section>
        <section class="contact-cta default real-media">
            <div class="inner-row">
                <div class="column-3 offset-1">
                    <img src="assets/images/rmhd-logo.png" data-rjs="2" alt="RealTimes"/>
                </div>
                <div class="column-7">
                    <a href="mailto:RMHD_Partners@realnetworks.com" class="link-btn" title="Contact">RMHD_Partners@realnetworks.com</a>
                </div>
            </div>
        </section>
        <section class="contact-cta listen">
            <div class="inner-row">
                <div class="column-3 offset-1">
                    <img src="assets/images/listen-logo.png" data-rjs="2" alt="RealTimes"/>
                </div>
                <div class="column-7">
                    <a href="mailto:Listen_Partners@realnetworks.com" class="link-btn" title="Contact">Listen_Partners@realnetworks.com</a>
                </div>
            </div>
        </section>
        <section class="contact-cta napster last">
            <div class="inner-row">
                <div class="column-3 offset-1">
                    <img src="assets/images/napster-home-logo.png" data-rjs="2" alt="RealTimes"/>
                </div>
                <div class="column-7">
                    <a href="mailto:api-inquiries@napster.com " class="link-btn" title="Contact">api-inquiries@napster.com </a>
                </div>
            </div>
        </section>
        <section class="contact-cta location">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>RealNetworks, Inc.</h1>
                    <address>1501 1st Avenue S.<br>
                        Suite 600<br>
                        Seattle, WA 98134<br>
                        Phone: 1-206-674-2700</address>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>