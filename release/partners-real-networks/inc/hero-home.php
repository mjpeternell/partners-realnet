<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="homeHero" class="hero resp-img">
    <img src="assets/images/home-header.jpg" data-rjs="2" class="img-responsive"/>
    <div class="caption">
        <h1>Life Inspired</h1>
        <h4>RealNetworks technology inspires people all over the globe to share, to listen,</br>to capture moments and to create amazing content. </h4>
        <a href="#" class="hero-btn" role="button">Be Inspired Video <i class="fa fa-play" aria-hidden="true"></i></a> 
    </div>
</section>