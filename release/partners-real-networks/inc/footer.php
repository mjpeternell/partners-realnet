<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!--<div class="wrapper-for-content-outside-of-footer">
   Uncomment this whole section for a sticky footer. The content of the page should be inside of this .wrapper-for-content-outside-of-footer
</div>-->

<footer class="footer-2" role="contentinfo">
    <div class="inner-row">
        <ul>
            <li class="nav-link">© 2017 RealNetworks, Inc.</li>
        </ul>
        <div class="footer-secondary-links">
            <ul>
                <li><a class="scroll-to-top" href="javascript:void(0)"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> Back to Top</a></li>
            </ul>
        </div>
</footer>
