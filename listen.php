
<?php
//Mobile/tablet/desktop detection.
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

//Check for Dev Environment if true load unminified scripts and css else load minified versions.
$minified = "";
//echo $myDomain = $_SERVER['HTTP_HOST'];
if ($_SERVER['HTTP_HOST'] === 'partners.realnetworks.com' || $_SERVER['HTTP_HOST'] === 'partners.mattpeternell.net') {
    $minified = ".min";
} else {
    $minified = "";
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>LISTEN - Real Partners - RealTimes for Mobile Operators, Device Makers, App Developers, Brands and Agencies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'inc/favicons.php'; ?>
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/theme-style<?php echo $minified; ?>.css">
        <script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <body class="<?php echo $deviceType ?>">
        <?php include 'inc/navigation.php'; ?>
        <?php include 'inc/hero-listen.php'; ?>
        <section class="page-intro no-divider">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <p>LISTEN by RealNetworks is the modern ringback service for popular music and advanced messaging <strong><em>plus</em></strong> a carrier-branded sales and marketing channel. LISTEN moves beyond simple ringback tones by using mobile call paths for productive and profitable messaging. </p>
                </div>
            </div>
        </section>
        <section class="listen-beyond-content teal">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Beyond the Top 40 </h1>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-4">
                        <img src="assets/images/listen-icon1.png" data-rjs="2" alt="Quote"/>
                        <p>Targeted advertising and subscriber specific messages</p>
                    </div>
                    <div class="column-4">
                        <img src="assets/images/listen-icon2.png" data-rjs="2" alt="Person"/>
                        <p>Custom status messages for specific callers</p>
                    </div>
                    <div class="column-4">
                        <img src="assets/images/listen-icon3.png" data-rjs="2" alt="Calendar"/>
                        <p>Calendar integration with pre-set status messages during busy times</p>
                    </div>
                </div>
                <div class="column-10 offset-1">

                    <div class="column-4">
                        <img src="assets/images/listen-icon4.png" data-rjs="2" alt="Location"/>
                        <p>Location detection with a status message based on geographic location<p>
                    </div>
                    <div class="column-4">
                        <img src="assets/images/listen-icon5.png" data-rjs="2" alt="Drive Mode "/>
                        <p>Drive Mode to automatically detect when user is driving and send an automated message or text</p>
                    </div>
                    <div class="column-4">
                        <img src="assets/images/listen-icon6.png" data-rjs="2" alt="Music"/>
                        <p>Premium music catalog with thousands of popular artists</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="rmhd-design-content grey teal">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Monetize the mobile ring</h1>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>Boost revenue</h2>
                        <p>Popular ringback tones and advanced features such as Drive Mode and calendar integration drive incremental sales.</p>
                    </div>
                    <div class="column-5 offset-2">
                        <h2>Provide modern smartphone services</h2>
                        <p>Allow subscribers to quickly and easily customize their callers' experiences using mobile call paths.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>Target customer communications</h2>
                        <p>Deliver customized account offers and targeted advertising based on subscriber account status.</p>
                    </div>
                    <div class="column-5 offset-2">
                        <h2>Get up-and-running quickly</h2>
                        <p>Take advantage of easy implementation and flexible, affordable partnerships.</p>
                    </div>
                </div>
                <div class="column-10 offset-1">
                    <div class="column-5">
                        <h2>Count on our expertise</h2>
                        <p>RealNetworks is the reliable, proven partner for consumer digital media with decades of innovations.</p>
                    </div>
                    <div class="column-5 offset-2">
                        &nbsp;
                    </div>
                </div>
            </div>
        </section>
        <section class="rmhd-audience-content">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <img src="assets/images/listen-icon7.png" data-rjs="2" alt="Gears Icon"/>
                    <h2>Mobile operators and MVNOs</h2>
                    <p>Offer advanced ringback messaging to increase revenue and get a profitable new way to communicate with customers.</p>
                </div>
            </div>
        </section>
        <section class="questions-cta listen">
            <div class="inner-row">
                <div class="column-10 offset-1">
                    <h1>Have Questions?</h1>
                    <div class="copy">Learn more about modern ringback messaging from LISTEN. Email <a href="mailto:Listen_Partners@realnetworks.com">Listen_Partners@realnetworks.com</a>. </div>
                </div>
            </div>
        </section>
        <?php include 'inc/footer.php'; ?>
        <script type="text/javascript" src="assets/js/vendor/retina.min.js"></script>
        <script src="assets/js/plugins<?php echo $minified; ?>.js"></script>
        <script src="assets/js/theme<?php echo $minified; ?>.js"></script>

        <?php //include_once 'inc/google-analytics.php';  ?>
    </body>
</html>