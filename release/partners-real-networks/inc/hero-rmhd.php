<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="homeHero" class="hero alt rmhd-gradient">
    <img src="assets/images/rmhd-header.png" data-rjs="2" class="img-responsive"/>
    <div class="caption">
        <h1><img src="assets/images/realmediahd-logo-white.png" data-rjs="2" alt="RealMedia HD"/></h1>
        <h4>Play tomorrow’s content on today’s networks and devices</h4>
        <h5>High-efficiency mobile video compression</h5>
    </div>
</section>